package dev.garstecki.pdf

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
private class Controller(
    private val service: DocumentService
) {
    @PostMapping(produces = [MediaType.APPLICATION_PDF_VALUE])
    suspend fun getPDF(@RequestBody html: String): ByteArray {
        return service.buildPdf(html)
    }
}