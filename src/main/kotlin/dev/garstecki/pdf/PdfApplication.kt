package dev.garstecki.pdf

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(DocumentProperties::class)
class PdfApplication

fun main(args: Array<String>) {
	runApplication<PdfApplication>(*args)
}
