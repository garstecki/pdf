package dev.garstecki.pdf

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "pdf")
internal data class DocumentProperties(
    val poolSize: Int = 4
)
