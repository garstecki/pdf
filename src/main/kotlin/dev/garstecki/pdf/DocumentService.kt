package dev.garstecki.pdf

import com.microsoft.playwright.Playwright
import jakarta.annotation.PreDestroy
import kotlinx.coroutines.*
import org.slf4j.LoggerFactory
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service
import java.util.concurrent.Executors
import java.util.concurrent.ThreadFactory
import java.util.concurrent.atomic.AtomicInteger


@Service
internal class DocumentService(
    private val properties: DocumentProperties
) {
    private val logger = LoggerFactory.getLogger(this::class.java)

    private val dispatcher = Executors.newFixedThreadPool(properties.poolSize, DocumentThreadFactory())
        .asCoroutineDispatcher()

    private val playwright = ThreadLocal.withInitial {
        logger.info("Starting new Playwright instance")

        Playwright.create().chromium().launch().newPage()
    }

    suspend fun buildPdf(html: String): ByteArray = withContext(dispatcher) {
        val playwright = playwright.get()
        val page = playwright.also { it.setContent(html) }

        logger.info("Started PDF build based on HTML: $html")
        page.pdf()
    }

    @EventListener(ApplicationStartedEvent::class)
    private suspend fun initializePool() = withContext(dispatcher) {
        repeat(properties.poolSize) { launch { playwright.get() } }
    }

    @PreDestroy
    private fun closeDispatcher() {
        dispatcher.close()
    }
}

private class DocumentThreadFactory : ThreadFactory {
    private val counter = AtomicInteger(0)

    override fun newThread(r: Runnable): Thread {
        return Thread(r).apply {
            name = "pdf-${counter.incrementAndGet()}"
        }
    }
}