package dev.garstecki.pdf

import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class DocumentServiceTest {

    @Test
    fun `can produce pdf`() = runTest {
        val service = DocumentService(DocumentProperties(4))

        val pdf = service.buildPdf("<h1>Title</h1>")

        assertTrue(pdf.isNotEmpty(), "Produced PDF document should not be empty")
    }
}