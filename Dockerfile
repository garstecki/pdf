FROM mcr.microsoft.com/playwright:v1.40.0-jammy

RUN apt-get update && apt-get install -y software-properties-common
RUN apt-get install -y openjdk-21-jdk

ENV JAVA_HOME /usr/lib/jvm/java-21-openjdk-amd64